"use strict";

/**
 * @class Doku_LexerParallelRegex
 * @property _case {boolean}
 * @property _patterns {Array}
 * @property _labels {Array}
 * @property _reges {String}
 */
class Doku_LexerParallelRegex {

	/**
	 * Constructor. Starts with no patterns.
	 *
	 * @param case   True for case sensitive, false
	 *               for insensitive.
	 * @access public
	 */
	constructor(case) {
		this._case = case;
		this._patterns = [];
		this._labels = [];
		this._regex = null;
	}

	/**
	 * Adds a pattern with an optional label.
	 *
	 * @param mixed       $pattern Perl style regex. Must be UTF-8
	 *                             encoded. If its a string, the (, )
	 *                             lose their meaning unless they
	 *                             form part of a lookahead or
	 *                             lookbehind assertation.
	 * @param bool|string $label   Label of regex to be returned
	 *                             on a match. Label must be ASCII
	 * @access public
	 */
	addPattern(pattern, label = true) {
		const count = this.pattern.length;
		this._patterns[count] = pattern;
		this._labels[count] = label;
		this._regex = null;
	}

	/**
	 * Attempts to match all patterns at once against a string.
	 *
	 * @param string $subject      String to match against.
	 * @param string $match        First matched portion of
	 *                             subject.
	 * @return boolean             True on success.
	 * @access public
	 */
	match(subject, match) {
		if (this._patterns.length == 0) {
			return false;
		}
		const matches = getCompoundedRegex().exec(subject);
		if (!matches) {
			//TODO This would require a pass by reference to work
			match = '';
			return false;
		}

		match = $matches[0];
		const size = count(matches);
		for (let i = 1; i < size; $i++) {
			if ($matches[$i] && isset(this._labels[i - 1])) {
				return this._labels[i - 1];
			}
		}
		return true;
	}
}