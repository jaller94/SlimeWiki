NODEFLAGS=--harmony-async-await

run:
	node $(NODEFLAGS) main.js

screen:
	source ~/.profile
	screen node $(NODEFLAGS) main.js

install:
	npm install

clean:
	rm -Rf node_modules

