"use strict";

const fs = require('fs');

function readFilePromised(filename) {
	return new Promise(function (resolve, reject) {
		fs.readFile(filename,{'encoding':'utf8'}, (err, data) => {
			if (err) {
				reject(err);
			}
			resolve(data);
		});
	});
}


function promiseConf(path) {
	return new Promise((resolve, reject) => {
		const promise = readFilePromised(path);

		promise.then((file) => {
			const re = /^([^#\n][^ ]+) +(.+)$/gm;
			const conf = {};

			let match;
			while ((match = re.exec(file)) !== null) {
				const key = match[1].trim();
				const value = match[2].trim();

				conf[key] = value;
				console.log('Read conf: ' + key + ' -> ' + value);
			}
			resolve(conf);
		});
	});
}

module.exports = promiseConf;
