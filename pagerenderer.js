"use strict";

const fs = require('fs');
const logger = require('./logger');
const path = require('path');

const dokuwiki2html = require('./www/shared/syntax');


function writeFilePromised(filename, data) {
	return new Promise(function (resolve, reject) {
		fs.writeFile(filename, data, (err, data) => {
			if (err) {
				reject(err);
			}
			resolve();
		});
	});
}

function pagerenderer(conf, page) {
	dokuwiki2html(page.source, (err, output) => {
		if (err) {
			logger(err, 'Failed to convert a page to HTML.');
		}
		const promise = writeFilePromised(path.join(conf.savedir, conf.cachedir, page.name.join(':') + ".html"), output);
		promise.then((res) => {
			logger(null, 'Cache saved!');
		}).catch((err) => {
			logger(err, 'Failed to store a page in the HTML cache.');
		});
	});
}

module.exports = pagerenderer;
