"use strict";

const fs = require('fs');
const path = require('path');
const url = require('url');
const pagerenderer = require('./pagerenderer');
const confreader = require('./confreader');

const promise = confreader('conf/mime.conf');

promise.then((mime) => {
	console.log(mime);
}).catch((err) => {
	console.log(err);
});

const conf_default = require('./conf/default.json');
const conf_local = require('./conf/local.json');

const conf = Object.assign({}, conf_default, conf_local);

const index = {};
index.pages= [];

function pagename2path(pagename) {
	if (Array.isArray(pagename)) {
		pagename = pagename.join('/');
	}
	return path.join(conf.savedir, conf.datadir, pagename + '.txt');
}

function readFilePromised(filename) {
	return new Promise(function (resolve, reject) {
		fs.readFile(filename,{'encoding':'utf8'}, (err, data) => {
			if (err) {
				reject(err);
			}
			resolve(data);
		});
	});
}

function writeFilePromised(filename, data) {
	return new Promise(function (resolve, reject) {
		fs.writeFile(filename, data, (err, data) => {
			if (err) {
				reject(err);
			}
			resolve();
		});
	});
}

function page_save(page) {
	console.log(page);
	if (page.source) {
		let pagepath = path.join(conf.savedir, conf.datadir);
		// Create namespaces as folders
		for (let i = 0; i < page.name.length - 1; i++) {
			pagepath = path.join(pagepath, page.name[i]);
			try {
				fs.mkdirSync(pagepath);
			} catch (err) {
				if (err.code=="EEXIST") {
					console.log("Server wanted to create a folder that already exists.");
				} else {
					throw err;
				}
			}
		}
		// Write page
		const promise = writeFilePromised(pagename2path(page.name), page.source);
		promise.then((res) => {
			console.log('File saved!');
		}).catch((err) => {
			console.log(err);
		});
		pagerenderer(conf, page);
	} else {
		// Delete page because the content is empty.
		fs.unlink(pagename2path(page.name), (err) => {
			if (err) {
				if (err.code=="ENOENT") {
					console.log("Client wanted to delete a page that does not exist.");
				} else {
					throw err;
				}
			} else {
				console.log('File deleted!');
			}
		});
	}
}

function page_receive(request, response) {
	const reg = /^\/api\/write\/(.*)(\?(.*))?/;
	let pagename = reg.exec(decodeURI(request.url))[1];
	pagename = (pagename || 'start'); //TODO This should be blocked
	console.log(pagename);
	let body = '';
	request.on('data', function (data) {
		body += data;
		// 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
		if (body.length > 1e6) {
			// FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
			request.connection.destroy();
		}
	});
	request.on('end', function () {
		console.log('POST DATA');
		console.log(body);
		let post = JSON.parse(body);
		let page = {
			name: pagename.split(':'),
			source: post.source
		};
		page_save(page);
		response.end('success');
	});
}

function page_send(request, response) {
	const pagename = /^\/api\/read\/(.*)(\?(.*))?/.exec(decodeURI(request.url))[1];
	const filepath = pagename2path(pagename);
	console.log('Read file from %s', filepath);
	const promise = readFilePromised(filepath);
	promise.then((data) => {
		response.end(data);
	}).catch((err) => {
		if (err.code=="ENOENT") {
			response.statusCode = 404;
			response.end('404');
		} else {
			console.log('Unknown error while reading a page.');
			console.log(err);
		}
	});
}

//We need a function which handles requests and send response
function handleRequest(request, response){
	let path = url.parse(request.url);
	const path_api = "api";
	if (request.url.startsWith('/' + path_api + '/write')) {
		if (request.method == 'OPTIONS') {
			response.end('');
		}
		if (request.method == 'POST') {
			page_receive(request, response);
		}
	} else if (request.url.startsWith('/' + path_api + '/read')) {
		if (request.method == 'OPTIONS') {
			response.end('');
		}
		if (request.method == 'GET') {
			page_send(request, response);
		}
	} else {
		response.statusCode = 403;
		response.end('403');
	}
}

module.exports = handleRequest;
