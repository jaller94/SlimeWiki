"use strict";

const http = require('http');
const handler_data = require('./handlerdata');
const handler_web = require('./handlerweb');

//Lets define a port we want to listen to
const PORT=8080;

//Create a server
const server = http.createServer(handleRequest);

//We need a function which handles requests and send response
function handleRequest(request, response){
	// Website you wish to allow to connect
	response.setHeader('Access-Control-Allow-Origin', '*');

	// Request methods you wish to allow
	response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	response.setHeader('Access-Control-Allow-Credentials', true);

	try {
		console.log('Request: %s %s', request.method, request.url);
		if (request.url.startsWith('/api/')) {
			handler_data(request, response);
		} else {
			handler_web(request, response);
		}
	} catch(err) {
		console.log(err);
	}
}

//Lets start our server
server.listen(PORT, function(){
	//Callback triggered when server is successfully listening. Hurray!
	console.log('Server listening on port %s!', PORT);
});
