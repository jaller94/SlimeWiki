"use strict";

const fs = require('fs');
const pagerenderer = require('./pagerenderer');
const path = require('path');
const pug = require('pug');
const url = require('url');

function handleRequest(request, response) {
	const requestedUrl = url.parse(request.url);
	let filePath = path.join('./www', requestedUrl.pathname);

	const pagelink_regex = /^\/.*\//;
	if (!pagelink_regex.test(requestedUrl.pathname)) {
		// It's some kind page! Let's deal with it separately!
		handlePageRequest(request, response);
		return;
	}

	// It's a file we'll want to provide.

	let extname = path.extname(filePath);
	let contentType = 'text/html';
	switch (extname) {
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.css':
			contentType = 'text/css';
			break;
		case '.json':
			contentType = 'application/json';
			break;
		case '.gif':
			contentType = 'image/gif';
			break;
		case '.jpg':
			contentType = 'image/jpg';
			break;
		case '.png':
			contentType = 'image/png';
			break;
		case '.webp':
			contentType = 'image/webp';
			break;
		case '.wav':
			contentType = 'audio/wav';
			break;
	}

	fs.readFile(filePath, function(error, content) {
		if (error) {
			if(error.code == 'ENOENT'){
				fs.readFile('./www/404.html', function(error, content) {
					response.writeHead(404, { 'Content-Type': contentType });
					response.end(content, 'utf-8');
				});
			}
			else {
				response.writeHead(500);
				response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
				response.end(); 
			}
		} else {
			// Website you wish to allow to connect
			response.setHeader('Access-Control-Allow-Origin', '*');

			// Request methods you wish to allow
			response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

			// Request headers you wish to allow
			response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

			// Set to true if you need the website to include cookies in the requests sent
			// to the API (e.g. in case you use sessions)
			response.setHeader('Access-Control-Allow-Credentials', true);

			response.writeHead(200, { 'Content-Type': contentType });
			response.end(content, 'utf-8');
		}
	});
}

function handlePageRequest(request, response) {
	// Parse URL and parse the query part (?those=arguments) as an object
	const requestedUrl = url.parse(request.url, true);

	const options = {'pretty': true};

	// Load config
	const conf_default = require('./conf/default.json');
	const conf_local = require('./conf/local.json');

	const ctx = {};
	ctx.html = '<p>Loading</p>';
	ctx.html += '<noscript><p>JavaScript disabled! It&quot;s required for now.</p></noscript>';

	const conf = Object.assign({}, conf_default, conf_local);
	conf.context = ctx;

	// Set context
	// Remove leading slash and convert URI characters
	let pagepath = requestedUrl.pathname.slice(1);
	pagepath = decodeURIComponent(pagepath).toLowerCase();
	conf.pagepath = pagepath;
	const namespaces = conf.pagepath.split(':');
	conf.pagename = namespaces.pop();

	conf.pagenamespaces = namespaces;
	if (conf.pagename == '') {
		conf.pagename = conf.start;
	}

	conf.keywords = conf.pagename.split(':').join(',');

	if (!Object.is(requestedUrl.query, null) && requestedUrl.query.do) {
		if (requestedUrl.query.do == 'index') {
			conf.error = 'The sitemap might be faulty!';

			const promise = promiseNamespaceList('');

			promise.then((pages) => {
				ctx.html = generateIndex(pages, '');

				console.log('options: ' + options);
				console.log('conf: ' + conf);

				const html = pug.renderFile('./www/index.pug', Object.assign({}, options, conf));
				response.writeHead(200, { 'Content-Type': 'text/html' });
				response.end(html, 'utf-8');
			//}).catch((err) => {
				//servePrettyErrorPage(request, response, err);
			}).catch((err) => {
				serveBasicErrorPage(request, response, err);
			});

			return;
		} else {
			conf.error = 'Command unknown: ' + requestedUrl.query.do;
		}
	} else {
		console.log('no');
	}

	const html = pug.renderFile('./www/index.pug', Object.assign({}, options, conf));
	response.writeHead(200, { 'Content-Type': 'text/html' });
	response.end(html, 'utf-8');
}

function promiseNamespaceList(namespace) {
	namespace = namespace || [];

	return new Promise((resolve, reject) => {
		fs.readdir(path.join('./data/pages/', namespace.join('/')), (err, files) => {
			if (err) {
				reject(err);
			}
			const pages = [];
			const subpromises = [];
			for(const key in files) {
				let pagename = files[key];
				// Determine if it is a file=page or a folder=namespace
				if (pagename.endsWith('.txt')) {
					pagename = pagename.slice(0,-4);
					pages.push( { name: pagename } );
				} else {
					// Start a new promise for the namespace
					const promise = promiseNamespaceList(namespace.concat([pagename]));
					promise.then((children) => {
						pages.push( { name: pagename, children: children } );
					}).catch((err) => {
						reject(err);
					});

					// Add it to the list of promises that have to be resolved before we can resolve ourselves
					subpromises.push(promise);
				}
			}

			// Wait for all namespaces to be resolved
			Promise.all(subpromises).then(() => {
				resolve(pages);
			}).catch((err) => {
				reject(err);
			});
		});
	});
}

function servePrettyErrorPage(request, response, error) {
	conf.error = '500 - Something went wrong!';

	const html = pug.renderFile('./www/index.pug', Object.assign({}, options, conf));
	response.writeHead(500, { 'Content-Type': 'text/html' });
	response.end(html, 'utf-8');
}

function serveBasicErrorPage(request, response, error) {
	response.writeHead(500, { 'Content-Type': 'text/html' });
	response.end('<h1>500 - Internal Server Error</h1><p>Try the <a href="/">homepage</a>! It might be safer there!</p><h2>Message:</h2><pre>' + error + '</pre>', 'utf-8');
}

function generateIndex(pages, namespace) {
	pages = pages || [];
	namespace = namespace || '';

	let html = '<ul>';
	for(const index in pages) {
		if (pages.hasOwnProperty(index)) {
			const item = pages[index];
			html += '<li>';
			if (item.hasOwnProperty('children')) {
				html += '<a href="/' + namespace + item.name + ':' + '">' + item.name + '</a>';
				html += generateIndex(item.children, namespace + item.name + ':');
			} else {
				html += '<a href="/' + namespace + item.name + '">' + item.name + '</a>';
			}
			html += '</li>';
		}
	}
	html += '</ul>';
	return html;
}

module.exports = handleRequest;
