"use strict";

function log(err, msg) {
	console.log('[LOGGER]: ' + (msg || 'undefined'));
	if (err) {
		console.log(err);
	}
}

module.exports = log;
