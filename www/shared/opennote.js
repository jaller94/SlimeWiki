/**
 * Builds an HTML string that displays the time and includes a <time>-tag for the semantic web.
 * @param date An JavaScript Date object
 * @returns {string} HTML code
 */
function opennote_dateToHTML(date) {
	//const display = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDay() + ' '
	//		+ date.getHours() + ':' + date.getMinutes();
	const display = date.toLocaleString();
	return '<time datetime="' + date.toISOString() + '">' + display + '</time>';
}

function opennote_validPageName(pagename) {
	//TODO Support missing for umlauts, other character sets an emojis
	return /^[\w-]+(:[\w-]+)*$/.test(pagename);
}

function opennote_wikify(pagename) {
	//TODO Lots of special characters don't get caught: eg. ! $ { }
	return pagename.replace(/\//g,'_').trim();
}

function opennote_pagename2path(pagename) {
	return opennote_wikify(pagename).replace(/:/g,'/');
}
