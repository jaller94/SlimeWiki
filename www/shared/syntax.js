"use strict";

function interwikilink2html(wikicode, pagename, label) {
	let pageUrl = "UNKNOWN WIKI";
	let iwClass = "iw_unknown";
	if (wikicode == 'doku') {
		pageUrl = 'https://www.dokuwiki.org/' + pagename;
		iwClass = 'iw_doku';
	} else if (wikicode == 'wp') {
		pageUrl = 'https://en.wikipedia.org/wiki/' + pagename;
		iwClass = 'iw_wp';
	}

	label = label || pagename;

	return '<a href="' + pageUrl + '" title="' + pageUrl + '" class="interwiki ' +
		iwClass + '">' + label + '</a>';
}

function link2html(link, label) {
	const interwiki_link_regex = /(.+?)>(.+)/;
	const external_link_regex = /(https?:\/\/.+)/;
	const email_link_regex = /(.+?@.+?\..+?)/;

	if (interwiki_link_regex.test(link)) {
		const match = interwiki_link_regex.exec(link);
		const wikicode = match[1];
		const pagename = match[2];
		return interwikilink2html(wikicode, pagename, label);
	}
	if (external_link_regex.test(link)) {
		label = label || link;
		return '<a href="' + link + '" title="' + link + '">' + label + '</a>';
	}
	if (email_link_regex.test(link)) {
		label = label || link;
		return '<a href="mailto:' + link + '" title="' + link + '">' + label + '</a>';
	}
	label = label || link;
	//TODO This assumes all pages are lower case
	return '<a href="./' + link.toLowerCase() + '" title="' + link + '">' + label + '</a>';

	/*
	 // External links
	 text = text.replace(/\[\[(https?:\/\/.+?)\|(.+?)\]\]/g, '<a href="$1">$2</a>');
	 text = text.replace(/\[\[(https?:\/\/.+?)\]\]/g, '<a href="$1">$1</a>');

	 // Email addresses
	 text = text.replace(/\[\[(.+?@.+?\..+?)\|(.+?)\]\]/g, '<a href="mailto:$1">$2</a>');
	 text = text.replace(/\[\[(.+?@.+?\..+?)\]\]/g, '<a href="mailto:$1">$1</a>');

	 // Local links
	 text = text.replace(/\[\[([^\]]+?)\|(.+?)\]\]/g, '<a href="./$1">$2</a>');
	 text = text.replace(/\[\[(.+?)\]\]/g, '<a href="./$1">$1</a>');
	 */
}

function dokuwiki2html(text, callback) {
	// Sections
	text = text.replace(/======(.+?)======/gm, '<h1>$1</h1>');
	text = text.replace(/=====(.+?)=====/gm, '<h2>$1</h2>');
	text = text.replace(/====(.+?)====/gm, '<h3>$1</h3>');
	text = text.replace(/===(.+?)===/gm, '<h4>$1</h4>');
	text = text.replace(/==(.+?)==/gm, '<h5>$1</h5>');

	text = text.replace(/^  \* (.+)/gm, '<ul><li>$1</li></ul>');
	text = text.replace(/^    \* (.+)/gm, '<ul><li><ul><li>$1</li></ul></li></ul>');
	text = text.replace(/^  - (.+)/gm, '<ol><li>$1</li></ol>');

	//Text Formatting
	text = text.replace(/\*\*(.+?)\*\*/g, '<strong>$1</strong>');
	text = text.replace(/\/\/(.+?)\/\//g, '<em>$1</em>');
	text = text.replace(/\_\_(.+?)\_\_/g, '<em class="u">$1</em>');
	text = text.replace(/\'\'(.+?)\'\'/g, '<code>$1</code>');
	//text = text.replace(/<del>(.+)<\/del>/g, '<del>$1</del>');
	//text = text.replace(/<sub>(.+)<\/sub>/g, '<sub>$1</sub>');
	//text = text.replace(/<sup>(.+)<\/sup>/g, '<sup>$1</sup>');
	text = text.replace(/^> (.*)/gm, '<blockquote>$1</blockquote>');
	text = text.replace(/^>> (.*)/gm, '<blockquote><blockquote>$1</blockquote></blockquote>');
	text = text.replace(/^>>> (.*)/gm, '<blockquote><blockquote><blockquote>$1</blockquote></blockquote></blockquote>');

	// Interwiki links
	const link_regex = /\[\[([^(\]\])]+?)(\|([^(\]\])]+))?\]\]/g;
	let match;
	while ((match = link_regex.exec(text)) !== null) {
		const link = match[1];
		const label = match[3];
		const link_html = link2html(link, label);
		text = text.replace(match[0], link_html);
	}

	// Specials & Line breaks
	text = text.replace(/----/gm, '<hr>');

	// Text to HTML Conversions
	text = text.replace(/(\d+)x(\d+)/gm, '$1×$2');
	text = text.replace(/\(c\)/gm, '©');
	text = text.replace(/\(tm\)/gm, '™');
	text = text.replace(/\(r\)/gm, '®');

	// Smilies
	text = text.replace(/8-P/gm, '😎');
	text = text.replace(/:-D/gm, '😃');
	text = text.replace(/:-P/gm, '😛');
	text = text.replace(/:-X/gm, '🤐');
	text = text.replace(/FIXME/gm, '<img src="/images/smilies/fixme.gif" alt="Fix Me!">');
	text = text.replace(/DELETEME/gm, '<img src="/images/smilies/delete.gif" alt="Delete!">');

	text = text.replace(/\n\n/g, '<br><br>'); //TODO This should be paragraphing

	callback(null, text);
}

function markdown2html(text, callback) {
	// Sections
	text = text.replace(/^# (.+)/gm, '<h1>$1</h1>');
	text = text.replace(/^## (.+)/gm, '<h2>$1</h2>');
	text = text.replace(/^### (.+)/gm, '<h3>$1</h3>');
	text = text.replace(/^#### (.+)/gm, '<h4>$1</h4>');
	text = text.replace(/^##### (.+)/gm, '<h5>$1</h5>');
	text = text.replace(/^###### (.+)/gm, '<h6>$1</h6>');

	text = text.replace(/__(.+?)__/g, '<strong>$1</strong>');
	text = text.replace(/\*\*(.+?)\*\*/g, '<strong>$1</strong>');
	text = text.replace(/_(.+?)_/g, '<em>$1</em>');
	text = text.replace(/\*(.+?)\*/g, '<em>$1</em>');
	text = text.replace(/`(.+?)`/g, '<code>$1</code>');

	// External Link
	text = text.replace(/\[(.+?)\]\((https?:\/\/.+?)\)/g, '<a href="$2">$1</a>');
	// Internal Link
	text = text.replace(/\[(.+?)\]\((#.+?)\)/g, '<a href="#$2">$1</a>');
	// TODO The following are likely unoffical
	text = text.replace(/\[(.+?)\]\((:.+?)\)/g, '<a href="/$2">$1</a>');
	text = text.replace(/\[(.+?)\]\((.+?)\)/g, '<a href="./$2">$1</a>');

	// Specials & Line breaks
	text = text.replace(/---/g, '<hr>');
	text = text.replace(/  $/gm, '<br>'); // Two spaces at the end of a line

	text = text.replace(/\n\n/g, '<br><br>'); //TODO This should be paragraphing

	callback(null, text);
}

module.exports = dokuwiki2html;
