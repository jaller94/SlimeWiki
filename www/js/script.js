"use strict";

function getPageStorage() {
	return (window['opennote'])._PAGESTORAGE;
}

function getCurrentPage() {
	return getPageStorage().getPage((window['opennote'])._PAGE);
}

function getCurrentPageName() {
	return (window['opennote'])._PAGE;
}

function setCurrentPageName(pagename) {
	(window['opennote'])._PAGE = pagename;
}

function wikiSyntax2HTMLPromised(content, syntax) {
	return new Promise((resolve, reject) => {
		syntax = (syntax || 'dokuwiki');
		if (syntax == 'dokuwiki') {
			dokuwiki2html(content, function (err, res) {
				if (err) {
					reject(err);
					return;
				}
				resolve(res);
			})
		} else if (syntax == 'markdown') {
			markdown2html(content, function (err, res) {
				if (err) {
					reject(err);
					return;
				}
				resolve(res);
			})
		}
		reject(new Error('Unknown syntax'));
	});
}

function renderPage(source) {
	$('.pageId>span').text(getCurrentPageName());

	const div_page = $('<div/>');

	const promise = wikiSyntax2HTMLPromised(source, 'dokuwiki');

	promise.then((res) => {
		// Wrap parser output into a div
		div_page.append(res);
	}).catch((err) => {
		// Generate alternative message
		div_page.append('<h1>Parsing error</h1>');
		div_page.append('<p>I present you, the error message:</p>');
		div_page.append( $('<pre/>').text(err.name + "\n" + err.message + "\n" + err.fileName + ":" + err.lineNumber) );
	}).then(() => {
		// Insert into the page
		const content = $('#page_wrapper');
		content.empty();
		content.append(div_page);
	});
}

function loadPage(pagename) {
	const pagepromise = getPageStorage().promiseUpdatedPage(pagename);
	pagepromise.then((page) => {
		renderPage(page.getSource());
	}).catch((err) => {
		renderPage('====== This topic does not exist yet ======You\'ve followed a link to a topic that doesn\'t exist yet. If permissions allow, you may create it by clicking on “[edit]”.');
	});
}

function updateDocInfo() {
	const docInfo = $('.docInfo');
	const path = opennote_pagename2path(getCurrentPageName()) + '.txt';
	docInfo.html(path + ' · Last modified: ' + opennote_dateToHTML(new Date()));
}

function viewCurrentPage() {
	loadPage(getCurrentPageName());
	updateDocInfo();
}

function loadExternalHTMLPromised(path) {
	return new Promise((resolve, reject) => {
		const xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (this.status == 200) {
					resolve(xhr.responseText);
				} else {
					reject(xhr.responseText);
				}
			}
		};
		xhr.open('GET', path, true);
		xhr.send();
	});
}

/**
 * Constructs and shows the text editor interface.
 */
function showEditor() {
	const page = getCurrentPage();
	const promise = loadExternalHTMLPromised('/components/page-edit.html');
	promise.then((html) => {
		const wrapper = $('#page_wrapper');
		const inner = $(html);
		wrapper.empty();
		wrapper.append(inner);

		// Fill and focus text area
		//TODO make sure it's the right textarea
		const textarea = $('*[name=wikitext]');
		textarea.val(page.source);
		textarea.focus();

		const summary = $('*[name=summary]');

		interface_bindings_page_editor(inner, textarea, summary);
	}).catch((err) => {
		//TODO display error
	}).then(() => {
		updateDocInfo();
	});
}

function initOpenNote() {
	(window['opennote'])._PAGESTORAGE = new PageStorage();
	interface_bindings_nav_pagetools();

	let pagename = decodeURIComponent(window.location.pathname.slice(1));
	// No namespace and no page name -> jump to the default
	if (pagename == '') {
		pagename = 'start';
	}
	// The last character indicated this is a namespace without a page name.
	if (pagename.slice(-1) == ':') {
		pagename += 'start';
	}
	setCurrentPageName(pagename);

	const qs = (function(a) {
		if (a == "") return {};
		const b = {};
		for (let i = 0; i < a.length; ++i)
		{
			const p=a[i].split('=', 2);
			if (p.length == 1)
				b[p[0]] = "";
			else
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	})(window.location.search.substr(1).split('&'));

	if (typeof qs.do == 'undefined' || qs.do == '') {
		viewCurrentPage();
	}
}

if (document.readyState === 'loading') {
	document.addEventListener('DOMContentLoaded', initOpenNote, false);
} else {
	initOpenNote();
}
