"use strict";

class Page {
	constructor(json) {
		// defaults
		this.source = '';

		// import from JSON
		if (json) {
			this.json = json.source;
		}
		console.log("[Page]: New Page object constructed!");
	}

	getSource() {
		return this.source;
	}

	setSource(source) {
		console.log('[Page]: saving source (length: ' + source.length + ')');
		this.source = source;
	}

	toJSON() {
		return JSON.stringify({
			source: this.source
		});
	}
}


class PageStorage {
	constructor() {
		this.pages = {};
	}

	getPage(pagename) {
		console.log('[PageStorage]: Getting page %s!', pagename);
		console.log('[PageStorage]getPage(): Next line: this.pages');
		console.log(this.pages);
		let page = this.pages[pagename.trim()];
		if (!page) {
			console.log("[PageStorage]getPage(): Seems like this page does not exist!");
			page = new Page();
			this.pages[pagename] = page;
		}
		console.log("[PageStorage]getPage(): Returning!");
		return page;
	}

	promiseUpdatedPage(pagename) {
		return new Promise((resolve, reject) => {
			const pagepromise = this.promisePageFetch(pagename);
			pagepromise.then((page) => {
				resolve(page);
			}).catch((err) => {
				reject(err);
			})
		});
	}

	readPageFromLocalStorage(pagename) {
		return new Page( localStorage.getItem('page_' + pagename) );
	}

	writePageToLocalStorage(pagename) {
		try {
			localStorage.setItem('page_' + pagename, this.getPage(pagename).toJSON());
		} catch (err) {
			console.log('[PageStorage]: Failed to write %s into the localStorage because %s.', pagename, err.toString());
		}

	}

	promiseSourceFromWeb(pagename) {
		return new Promise((resolve, reject) => {
			const yourUrl = '/api/read/' + opennote_pagename2path(pagename);
			const xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function () {
				if (this.readyState == 4 && this.status == 200) {
					resolve(xhr.responseText);
				} else if (this.readyState == 4) {
					reject(this.status);
				}
			};
			xhr.open('GET', yourUrl, true);
			xhr.send();
		});
	}

	writeToWeb(pagename) {
		const page = this.getPage(pagename);
		const yourUrl = '/api/write/'+pagename;
		const xhr = new XMLHttpRequest();
		xhr.open('POST', yourUrl, true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(page.toJSON());
		viewCurrentPage();
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				console.log('Successfully wrote the page to the server!');
			} else if (this.readyState == 4) {
				alert('error: HTTP ' + this.status);
			}
		};
		return false;
	}

	/***
	 * Updates the page
	 * @param pagename
	 */
	promisePageFetch(pagename) {
		console.log('pagename:' + pagename);
		return new Promise((resolve, reject) => {
			const sourcepromise = this.promiseSourceFromWeb(pagename);
			sourcepromise.then((source) => {
				const page = this.getPage(pagename);
				page.setSource(source);
				console.log('Fetched page %s from the web!', pagename);
				this.writePageToLocalStorage(pagename);
				resolve(page);
			}).catch((err) => {
				console.log(err);
				const page = this.readPageFromLocalStorage(pagename);
				if (page && page.getSource() != "") {
					console.log('Fetched page %s from localstorage!', pagename);
					resolve(page);
				} else {
					reject();
				}
			})
		});
	}
}
