"use strict";

function interface_bindings_nav_pagetools() {
	document.getElementById('nav_view').addEventListener('click', (event) => {
		event.preventDefault();
		viewCurrentPage();
	});

	document.getElementById('nav_edit').addEventListener('click', (event) => {
		event.preventDefault();
		showEditor();
	});
}

function interface_bindings_page_editor(form, textarea, summary) {
	form.on('submit', function () {
		const page = getCurrentPage();
		console.log(textarea);
		page.setSource(textarea.val());
		getPageStorage().writeToWeb(getCurrentPageName());
		viewCurrentPage();
		return false;
	});
	console.log('editor interface bound');
}
